const startBtn = document.getElementById("start");
const pauseBtn = document.getElementById("pause");
const resetBtn = document.getElementById("reset");
const timerElement = document.getElementById("timer");
let minute = window.sessionStorage.getItem("min")
let second = window.sessionStorage.getItem("sec")

let sec = second ? second : 0;
let min = minute ? minute : 0;

timerElement.innerHTML = `${min.toString().padStart(2, "0")} : ${sec.toString().padStart(2, "0")}`;
let timer;

startBtn.addEventListener("click", function () {
    timer = setInterval(TimerHandler, 1000);
    isPaused = false
});

pauseBtn.addEventListener("click", function () {
    timer = clearInterval(timer);
    isPaused = true
    window.sessionStorage.setItem("min", min)
    window.sessionStorage.setItem("sec", sec)
});

resetBtn.addEventListener("click", function () {
    sec = 0
    min = 0
    timerElement.innerHTML = `${min.toString().padStart(2, "0")} : ${sec.toString().padStart(2, "0")}`;
    timer = clearInterval(timer);
    window.sessionStorage.removeItem("min") 
    window.sessionStorage.removeItem("sec")
});

function TimerHandler() {
    if (!isPaused) {
        sec++;

        if (sec == 60) {
            sec = 0;
            min++;
        }
        if (min == 60) {
            min = 0
            sec = 0
        }
    }

    sec = sec.toString().padStart(2, "0");
    min = min.toString().padStart(2, "0");

    timerElement.innerHTML = `${min} : ${sec}`;
}